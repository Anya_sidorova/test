<?php

namespace core;


class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);

    }

    public function run()
    {

        $uri = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode('/', $internalRoute);

                $controller = $segments[0];
                $action = $segments[1];

                $language = preg_split('/(?<=\\w)(?=[A-Z])/', $controller);
                $lang = $this->loadLanguage($language[0]);
                $controller = 'application\controllers\\' . $controller;
                $controller = new $controller($lang);
                $controller->$action();
            }
        }

    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '  /');
        }
    }


    /**
     * @param $file
     * @return string
     */
    public function loadLanguage($file)
    {
        $d = DIRECTORY_SEPARATOR;
        $file = mb_strtolower($file);

        return 'application' . $d . 'language' . $d . $_SESSION['language'] . $d . $file . '.php';
    }

}
