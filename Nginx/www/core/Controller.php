<?php

namespace core;

/**
 * Class Controller
 * @package core
 * @property $session
 * @property $language
 * @property $view \View
 */
class Controller
{
    public $session;
    public $language;
    public $view;

    public function __construct()
    {
        $this->session = $_SESSION;
        $this->view = new View();
        $this->language = $this->addLanguage();
    }

    /**
     * @return string
     */
    public function addLanguage()
    {
        if (isset($this->session['language'])) {
            return $this->session['language'];
        }

        $_SESSION['language'] = 'ru';

        return 'ru';
    }

    /**
     * @param $post
     * @return mixed|string
     */
    public function preparation($post)
    {
        $post = stripslashes($post);
        $post = htmlspecialchars(trim($post));
        $post = str_replace(['`', '%', '../', '\0'], '', $post);

        return $post;
    }
}
