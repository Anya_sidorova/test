<?php

namespace application\controllers;

use application\models\User;
use core\Controller;

class RegisterController extends Controller
{
    public $variables_language = [];
    private $errors = [];

    public function __construct($language_path)
    {
        parent::__construct();
        $this->variables_language = include($language_path);
    }

    public function actionRegistration()
    {
        $user_model = new User();

        if (isset($_POST)) {

            $first_name = $this->preparation($_POST['first_name']);
            $last_name = $this->preparation($_POST['last_name']);
            $email = $this->preparation($_POST['email']);
            $password = $this->preparation($_POST['password']);
            $confirm_password = $this->preparation($_POST['confirm_password']);

            if ($user_model->isEmpty($first_name)) {
                $this->errors['error_first_name'] = $this->variables_language['error_first_name'];
            }

            if ($user_model->isEmpty($last_name)) {
                $this->errors['error_last_name'] = $this->variables_language['error_last_name'];
            }

            if ($user_model->isEmpty($email)) {
                $this->errors['error_email'] = $this->variables_language['error_email'];
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->errors['error_email'] = $this->variables_language['error_email_cor'];
            }

            if ($user_model->isEmpty($password)) {
                $this->errors['error_password'] = $this->variables_language['error_password'];
            }

            if ($user_model->isEmpty($confirm_password)) {
                $this->errors['error_password'] = $this->variables_language['error_password'];
            }

            if ($password !== $confirm_password) {
                $this->errors['error_password'] = $this->variables_language['error_password_notcor'];
            }

            if (!count($this->errors)) {
                if (isset($_FILES['img'])) {
                    $this->uploadImages($_FILES['img']);
                }
                if (isset($_FILES['img']['name'])) {
                    $img_name = $_FILES['img']['name'];
                } else {
                    $img_name = null;
                }
                $id_user = $user_model->addUser($first_name, $last_name, $email, $password, $img_name);

                if ($id_user > 0) {
                    $_SESSION['user'] = $user_model->getUser($id_user);
                } else {
                    $this->errors[] = $this->variables_language['errors'];
                }
            }
            echo json_encode($this->errors);
        }
    }

    private function uploadImages($files)
    {
        $filePath = $files['tmp_name'];
        $errorCode = $files['error'];

        if ($errorCode !== UPLOAD_ERR_OK || !is_uploaded_file($filePath)) {
            $errorMessages = [
                UPLOAD_ERR_FORM_SIZE => $this->errors['error_img_max_size'] = $this->variables_language['error_img_max_size'],
                UPLOAD_ERR_NO_FILE => $this->errors['error_img_no_upload'] = $this->variables_language['error_img_no_upload'],
            ];

            $unknownMessage = $this->errors['error_img'] = $this->variables_language['error_img'];
            $outputMessage = isset($errorMessages[$errorCode]) ? $errorMessages[$errorCode] : $unknownMessage;
            $this->errors['error_img'] = $outputMessage;
        } else {
            $fi = finfo_open(FILEINFO_MIME_TYPE);
            $mime = (string)finfo_file($fi, $filePath);
            finfo_close($fi);

            if (strpos($mime, 'image') === false) {
                $this->errors['error_img'] = $this->variables_language['error_img_mime'];
            }

            if (!move_uploaded_file($filePath, 'upload' . DIRECTORY_SEPARATOR . $files['name'])) {
                $this->errors['error_img'] = $this->variables_language['error_img'];
            }
        }

        if (count($this->errors) > 0) {
            echo json_encode($this->errors);
        }
    }
}
