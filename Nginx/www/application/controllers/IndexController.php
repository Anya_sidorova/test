<?php

namespace application\controllers;

use core\Controller;

use application\models\User;

class IndexController extends Controller

{
    public $variables_language = [];
    private $errors = [];

    public function __construct($language_path)
    {
        parent::__construct();
        $this->variables_language = include($language_path);
    }

    function index()
    {

        if (isset($_SESSION['user'])) {
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/user');
        }

        $data = [];
        $data['lang'] = $this->language;
        $data['errors'] = $this->errors;

        //подгрузка языка
        $data['title'] = $this->variables_language['title'];
        $data['header'] = $this->variables_language['header'];
        $data['label_email'] = $this->variables_language['label_email'];
        $data['label_password'] = $this->variables_language['label_password'];
        $data['label_repassword'] = $this->variables_language['label_repassword'];
        $data['label_name'] = $this->variables_language['label_name'];
        $data['label_lastname'] = $this->variables_language['label_lastname'];
        $data['success'] = $this->variables_language['success'];
        $data['registration'] = $this->variables_language['registration'];
        $data['aith'] = $this->variables_language['aith'];
        $data['reg'] = $this->variables_language['reg'];
        $data['load_file'] = $this->variables_language['load_file'];

        //ошибки
        $data['error_email_auth'] = $this->variables_language['error_email_auth'];
        $data['error_email'] = $this->variables_language['error_email'];
        $data['error_email_cor'] = $this->variables_language['error_email_cor'];
        $data['error_password_notcor'] = $this->variables_language['error_password_notcor'];
        $data['error_password_auth'] = $this->variables_language['error_password_auth'];
        $data['error_password'] = $this->variables_language['error_password'];
        $data['error_first_name'] = $this->variables_language['error_first_name'];
        $data['error_last_name'] = $this->variables_language['error_last_name'];
        //        $data['error_img_type'] = $this->variables_language['error_img_type'];
        $data['error_img'] = $this->variables_language['error_img'];

        $data['errors'] = $this->variables_language['errors'];


        $this->view->generate('common/header', $data);
        $this->view->generate('common/main', $data);
        $this->view->generate('common/footer', $data);

    }
}
