<?php

namespace application\controllers;

use core\Controller;

class UserController extends Controller
{
    public $variables_language = [];

    public function __construct($language_path)
    {
        parent::__construct();
        $this->variables_language = include($language_path);
    }

    public function index()
    {
        if (!isset($_SESSION['user'])) {
            header('Location: http://' . $_SERVER['HTTP_HOST']);
        }

        $data['user_info'] = $_SESSION['user'];
        $data['logout'] = $this->variables_language['logout'];

        $this->view->generate('common/header', $data);
        $this->view->generate('common/user', $data);
        $this->view->generate('common/footer', $data);
    }

    public function logout()
    {
        unset($_SESSION['user']);
        header('Location: http://' . $_SERVER['HTTP_HOST']);
    }
}
