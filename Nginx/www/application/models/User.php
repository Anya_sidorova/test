<?php

namespace application\models;

use core\Db;

/**
 * Class User
 * @package application\models
 */
class User extends Db
{
    public $db;

    public function __construct()
    {
        $dbcon = new Db();
        $this->db = $dbcon::getConnection();
    }

    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function checkUserData($email, $password)
    {
        $sql = 'SELECT * FROM users WHERE email = :email';

        $result = $this->db->prepare($sql);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            if (password_verify($password, $user['password'])) {
                return $user['id'];
            }

            return false;
        }

        return false;
    }

    /**
     * @param $first_name
     * @param $last_name
     * @param $email
     * @param $password
     * @param $img_name
     * @return string
     */
    public function addUser($first_name, $last_name, $email, $password, $img_name)
    {

        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = 'INSERT INTO users SET first_name =:first_name, last_name =:last_name, email =:email, password =  :password, image =:img';
        $result = $this->db->prepare($sql);
        $result->bindParam(':first_name', $first_name, \PDO::PARAM_STR);
        $result->bindParam(':last_name', $last_name, \PDO::PARAM_STR);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->bindParam(':img', $img_name, \PDO::PARAM_LOB);
        $result->bindParam(':password', $password, \PDO::PARAM_STR);
        $result->execute();

        return $this->db->lastInsertId();
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getUser($user_id)
    {
        $sql = 'SELECT * FROM users where id = :user_id';
        $result = $this->db->prepare($sql);
        $result->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $result->execute();

        return $result->fetch($this->db::FETCH_ASSOC);
    }

    /**
     * @param $field
     * @return bool
     */
    public function isEmpty($field)
    {
        return $field === '';
    }

}
