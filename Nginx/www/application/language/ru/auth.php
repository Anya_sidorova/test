<?php
return [
    'error_email' => 'Поле "Email" обязательно для заполнения',
    'error_password' => 'Поле "Пароль" обязательно для заполнения',
    'errors' => 'Неправильные данные для входа на сайт',
];
