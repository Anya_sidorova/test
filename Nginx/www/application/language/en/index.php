<?php

return [

    'title' => 'Login',
    'header' => 'Log in to your account',
    'label_email' => 'Email',
    'label_name' => 'Firstname',
    'label_lastname' => 'Lastname',
    'label_password' => 'Password',
    'label_repassword' => 'Repeat password',
    'success' => 'Login',
    'registration' => 'Sign up',
    'aith' => 'Login',
    'reg' => 'Registration',
    'error_email' => 'The "Email" field is incorrect, the email address must be between 1 and 30 characters',
    'error_email_auth' => 'Email field is required',
    'error_email_cor' => 'Email field is invalid',
    'error_password' => 'Password field is required',
    'error_password_notcor' => 'Passwords do not match',
    'error_password_auth' => 'Password field is required',
    'error_first_name' => 'The "Name" field is filled out incorrectly, the name must be from 1 to 30 characters',
    'error_last_name' => 'The "Surname" field is filled out incorrectly, the surname must be from 1 to 30 characters',
    'error_img' => 'Picture must be format .png, .jpeg, .jpg, .gif',
    'errors' => 'Incorrect login details',
    'load_file' => 'Loading file',

];
