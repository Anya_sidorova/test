<?php
return [
    'error_email' => 'Email field is required',
    'error_password' => 'Password field is required',
    'errors' => 'Incorrect login details',
];
