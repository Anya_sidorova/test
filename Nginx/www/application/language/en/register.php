<?php

return [
    'error_email' => 'The "Email" field is incorrect, the email address must be between 1 and 30 characters',
    'error_email_auth' => 'Email field is required',
    'error_email_cor' => 'Email field is invalid',
    'error_password' => 'Password field is required',
    'error_password_notcor' => 'Passwords do not match',
    'error_password_auth' => 'Password field is required',
    'error_first_name' => 'The "Name" field is filled out incorrectly, the name must be from 1 to 30 characters',
    'error_last_name' => 'The "Surname" field is filled out incorrectly, the surname must be from 1 to 30 characters',
    'errors' => 'This user is already registered',

    //error_img    
    'error_img_type' => 'Picture must be format .png, .jpeg, .jpg, .gif',
    'error_img' => 'Error loading image',
    'error_img_mime' => 'Only images can be uploaded',
    'error_img_max_size' => 'Upload file size exceeds value MAX_FILE_SIZE',
    'error_img_no_upload' => 'File failed to load',
];
