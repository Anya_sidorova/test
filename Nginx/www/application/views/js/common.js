$(document).ready(function () {
  $('.registration_form').hide();

  $('.auth, .registr').on('click', function (e) {
    let target = $(e.target);

    $(this).addClass('active').siblings().removeClass('active');
    if (target.is('.auth')) {
      $('.authorization_form').show();
      $('.registration_form').hide();
    } else {
      $('.registration_form').show();
      $('.authorization_form').hide()
    }
  });

  $('.language').on('click', function () {
    let lang = $(this).text().toLowerCase();
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'language/edit',
      data: {
        language: lang
      },
      success: function (request) {
        window.location.reload();
      }
    });

  });

  $('.authorization_form').on('submit', function (event) {
    event.preventDefault();
    let email = $('input[type=\'email\']');
    let password = $('input[type=\'password\']');
    $('#errors').hide();
    let validateEmail = validate(email);
    let validatePassword = validate(password);
    if (validateEmail && validatePassword) {
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'index/auth',
        data: {
          email: email.val(),
          password: password.val()
        },
        success: function (result) {
          if (result !== null) {
            $('#errors').text(result);
            $('#errors').css('display', 'block');
          } else {
            window.location.href = '/user'
          }
        }
      });
    }
  });

  $('#logout').on('click', function () {
    $.ajax({
      type: 'post',
      url: 'user/logout',
      success: function () {
        window.location.href = '/';
      }
    })
  })
});

let file;

$('#file').change(function () {
  file = this.files;
});

$('#reg').on('click', function (event) {
  $('#errors').hide();
  let data = new FormData();
  event.stopPropagation();
  event.preventDefault();

  let check = true;

  if (file) {
    $.each(file, function (key, value) {
      if (value.type.match('image/jpg') || value.type.match('image/jpeg') || value.type.match('image/png') || value.type.match('image/gif')) {
        data.append('img', value);
      } else {
        check = false;
        alert("Картинка должна быть типа .jpg, .jpeg, .png, .gif");
      }
    });
  }

  let elems = {
    'first_name': $('#first_name'),
    'last_name': $('#last_name'),
    'email': $('#email'),
    'password': $('#password'),
    'confirm_password': $('#confirm_password'),
  };

  for (let key in elems) {
    if (validate(elems[key])) {
      data.append(key, elems[key].val());
    } else {
      check = false;
    }
  }

  if (check) {
    $.post({
      data: data,
      url: '/index/registration',
      processData: false,
      contentType: false,
      cache: false,
    })
      .done(function (response) {

        let result = JSON.parse(response);

        if (result.length === 0) {
          window.location.href = '/user';
        } else {
          for (let key in result) {
            $('#errors').text(result[key]);
            $('#errors').css('display', 'block');
          }
        }
      })
      .fail(function (response) {
        alert("Ошибка");
      })
  }
});

function validate(field) {
  if (field.val().length > 30 || field.val() === ''){
    field.siblings('small').css('display', 'block');
    return false;
  }else{
    field.siblings('small').css('display', 'none');
    return true;
  }
}

(function () {

  'use strict';

  $('.input-file').each(function () {
    let $input = $(this),
      $label = $input.next('.js-labelFile'),
      labelVal = $label.html();

    $input.on('change', function (element) {
      let fileName = '';
      if (element.target.value) fileName = element.target.value.split('\\').pop();
      fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
    });
  });

})();
