<section class="main_block">
	<div class="container ">
		<div class="row justify-content-center">
			<div class="col-lg-6 wrap-form">

				<ul class="row justify-content-center">
					<li class="col-lg-6 auth active text-center h3">
              <?= $data['aith'] ?>
					</li>
					<li class="col-lg-6 registr text-center h3">
              <?= $data['reg'] ?>
					</li>
				</ul>

				<div id="errors" class="alert alert-danger" role="alert">

				</div>

				<form action="" method="post" class="authorization_form">
					<div class="form-group">
						<label><?= $data['label_email'] ?></label>
						<input type="email" class="form-control" placeholder="-" name="email_auth">
						<small class="form-text text-danger"><?= $data['error_email'] ?></small>
					</div>
					<div class="form-group">
						<label><?= $data['label_password'] ?></label>
						<input type="password" class="form-control" placeholder="-" name="password_auth">
						<small class="form-text text-danger"><?= $data['error_password'] ?></small>
					</div>
					<input type="submit" name="auth" class="btn btn-dark" value="<?= $data['success'] ?>"/>
				</form>

				<form action="" method="post" class="registration_form" enctype="multipart/form-data">
					<div class="form-group">
						<label><?= $data['label_name'] ?></label>
						<input type="text" class="form-control" placeholder="-" name="first_name" id="first_name">
						<small class="form-text text-danger"><?= $data['error_first_name'] ?></small>
					</div>
					<div class="form-group">
						<label><?= $data['label_lastname'] ?></label>
						<input type="text" class="form-control" placeholder="-" name="last_name" id="last_name">
						<small class="form-text text-danger"><?= $data['error_last_name'] ?></small>
					</div>
					<div class="form-group">
						<label><?= $data['label_email'] ?></label>
						<input type="text" class="form-control" placeholder="-" name="email" id="email">
						<small class="form-text text-danger"><?= $data['error_email'] ?></small>
					</div>
					<div class="form-group">
						<label for=""><?= $data['label_password'] ?></label>
						<input type="password" class="form-control" placeholder="-" name="password" id="password">
						<small class="form-text text-danger"><?= $data['error_password'] ?></small>
					</div>
					<div class="form-group">
						<label for=""><?= $data['label_repassword'] ?></label>
						<input type="password" class="form-control" placeholder="-" name="confirm_password" id="confirm_password">
						<small class="form-text text-danger"><?= $data['error_password'] ?></small>
					</div>
					<div class="form-group load_file">
						<input type="file" name="img" id="file" class="input-file">
						<label for="file" class="btn btn-tertiary js-labelFile">
							<i class="icon fa fa-check"></i>
							<span class="js-fileName"><?= $data['load_file'] ?></span>
						</label>
						<small class="form-text text-danger"><?= $data['error_img'] ?></small>
					</div>
					<input id="reg" type="submit" name="regData" class="btn btn-dark mt-4" value="<?= $data['registration'] ?>"/>
				</form>
			</div>
		</div>
	</div>
</section>
