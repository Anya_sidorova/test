<div class="container pro_file">
	<div class="row align-items-center justify-content-between mt-5">
		<div class="col-xs-12 col-sm-6 col-md-3">
        <?php if (!empty($data['user_info']['image'])) { ?>
					<img src="../upload/<?= $data['user_info']['image'] ?>" alt="изображение профиля">
        <?php } else { ?>
					<img src="/application/views/img/user.png" alt="изображение профиля">
        <?php } ?>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-9">
			<h2><?= $data['user_info']['first_name'] ?>&nbsp;<?= $data['user_info']['last_name'] ?></h2>
			<h5><?= $data['user_info']['email'] ?></h5>
			<button type="button" id="logout" class="btn btn-dark mt-2">
          <?= $data['logout'] ?>
			</button>
		</div>
	</div>
</div>
