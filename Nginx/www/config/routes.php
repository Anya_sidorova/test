<?php

return [
    'index/auth' => 'AuthController/actionAuth',
    'index/registration' => 'RegisterController/actionRegistration',

    'user' => 'UserController/index',
    'user/logout' => 'UserController/logout',

    '^$' => 'IndexController/index',
    
    'language/edit' => 'LanguageController/edit',
];
